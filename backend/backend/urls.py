from django.urls import path, include
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('leads.urls')),
    path('api-token-auth/', obtain_jwt_token),
]
